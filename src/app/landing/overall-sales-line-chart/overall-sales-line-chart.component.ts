
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-overall-sales-line-chart',
  templateUrl: './overall-sales-line-chart.component.html',
  styleUrls: ['./overall-sales-line-chart.component.css']
})

export class OverallSalesLineChartComponent implements OnInit {


  constructor() { }

  ngOnInit() {
  }

  chartOptions = {
    responsive: true,
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',

          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };

  chartData = [
    { data: [330, 600, 260, 700], label: 'Week' },
    { data: [120, 455, 100, 340], label: 'Month' },
    { data: [45, 67, 800, 500], label: 'Annual' }
  ];

  chartLabels = ['January', 'February', 'Mars', 'April'];

  onChartClick(event) {
    console.log(event);
  }
}
