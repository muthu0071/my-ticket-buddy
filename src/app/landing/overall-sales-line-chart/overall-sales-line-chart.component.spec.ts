import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverallSalesLineChartComponent } from './overall-sales-line-chart.component';

describe('OverallSalesLineChartComponent', () => {
  let component: OverallSalesLineChartComponent;
  let fixture: ComponentFixture<OverallSalesLineChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverallSalesLineChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverallSalesLineChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
