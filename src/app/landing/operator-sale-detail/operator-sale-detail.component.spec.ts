import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperatorSaleDetailComponent } from './operator-sale-detail.component';

describe('OperatorSaleDetailComponent', () => {
  let component: OperatorSaleDetailComponent;
  let fixture: ComponentFixture<OperatorSaleDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperatorSaleDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperatorSaleDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
