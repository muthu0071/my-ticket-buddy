import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingRoutingModule } from './landing-routing.module';
import { LandingComponent } from './landing.component';
import { SharedModuleModule } from '../shared-module/shared-module.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AccountsComponent } from './accounts/accounts.component';
import { RouterModule } from '@angular/router';
import { OverallSalesLineChartComponent } from './overall-sales-line-chart/overall-sales-line-chart.component';

import { ChartsModule } from 'ng2-charts';
import { OperatorSaleDetailComponent } from './operator-sale-detail/operator-sale-detail.component';

@NgModule({
  declarations: [LandingComponent, DashboardComponent, AccountsComponent, OverallSalesLineChartComponent, OperatorSaleDetailComponent],
  imports: [
    CommonModule,
    LandingRoutingModule,
    SharedModuleModule,
    RouterModule,
    ChartsModule
  ]
})
export class LandingModule { }
